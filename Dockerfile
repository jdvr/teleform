FROM golang:1.16-alpine

RUN apk add gcc musl-dev postgresql-client

WORKDIR /go/src/app
COPY . .

RUN go build teleform.go
RUN go build init-db.go

CMD ["./teleform"]