package dto

import "time"

type Field struct {
	Id    string `json:"id"`
	Title string `json:"title"`
}

type AnswerChoice struct {
	Label *string `json:"label,omitempty"`
	Other *string `json:"other,omitempty"`
}

type AnswerChoices struct {
	Labels []string `json:"labels"`
}

type AnswerPayment struct {
	Amount         string `json:"amount,omitempty"`
	LastFourDigits string `json:"last4,omitempty"`
	Name           string `json:"name,omitempty"`
	Success        *bool  `json:"success,omitempty"`
}
type Answer struct {
	Type        string         `json:"type"`
	Text        *string        `json:"text,omitempty"`
	URL         *string        `json:"url,omitempty"`
	Email       *string        `json:"email,omitempty"`
	PhoneNumber *string        `json:"phone_number,omitempty"`
	Date        *string        `json:"date,omitempty"`
	FileURL     *string        `json:"file_url,omitempty"`
	Number      *int64         `json:"number,omitempty"`
	Boolean     *bool          `json:"boolean,omitempty"`
	Choice      *AnswerChoice  `json:"choice,omitempty"`
	Choices     *AnswerChoices `json:"choices,omitempty"`
	Payment     *AnswerPayment `json:"payment,omitempty"`
	Field       *Field         `json:"field"`
}

type Form struct {
	Id     string   `json:"id"`
	Title  string   `json:"title"`
	Fields []*Field `json:"fields"`
	Hidden []string `json:"hidden,omitempty"`
}

type Response struct {
	FormID      string                 `json:"form_id"`
	Token       string                 `json:"token"`
	LandedAt    *time.Time             `json:"landed_at,omitempty"`
	SubmittedAt *time.Time             `json:"submitted_at,omitempty"`
	Hidden      map[string]interface{} `json:"hidden,omitempty"`
	Form        *Form                  `json:"definition,omitempty"`
	Answers     []*Answer              `json:"answers"`
}

type Event struct {
	ID       string    `json:"event_id,omitempty"`
	Type     string    `json:"event_type,omitempty"`
	Response *Response `json:"form_response"`
}
