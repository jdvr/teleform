package server

import (
	"teleform/bot"
	"teleform/dao"
	"teleform/server/handler"
	"teleform/server/middleware"

	"github.com/gin-gonic/gin"
)

func CreateEngine(sender bot.MessageSender, webhookDao dao.WebhookDao) *gin.Engine {

	router := gin.Default()
	router.Use(middleware.RequestLoggerMiddleware())
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	router.POST("/new_response/", handler.CreateOnNewResponseHandler(webhookDao, sender))
	router.POST("/init_db/:key", handler.CreateInitDbHandler())
	return router
}
