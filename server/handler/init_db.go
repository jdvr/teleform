package handler

import (
	"net/http"
	"teleform/infra/config"
	"teleform/infra/postgress"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

func CreateInitDbHandler() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		key := ctx.Param("key")
		if key != config.GetInitDbKey() {
			ctx.AbortWithStatus(http.StatusBadRequest)
			return
		}
		err := postgress.CreateDb(config.GetDatabaseURL())
		if err != nil {
			log.Error("Error creating DB")
			log.Error(err)
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"status": "error",
				"error":  err.Error(),
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"status": "ok",
		})
	}
}
