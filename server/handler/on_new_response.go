package handler

import (
	"fmt"
	"strings"
	"teleform/bot"
	"teleform/dao"
	"teleform/server/dto"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

func CreateOnNewResponseHandler(webhookDao dao.WebhookDao, sender bot.MessageSender) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var event dto.Event

		if parseBodyErr := ctx.BindJSON(&event); parseBodyErr != nil {
			log.Errorf("Pasing body error => %v", parseBodyErr)
			ctx.JSON(400, gin.H{
				"status":  "FAIL",
				"message": "error parsing body",
			})
			return
		}
		log.Debugf("Parsed event %v", event)
		webhook, exists := webhookDao.FindByFromId(event.Response.FormID)
		if !exists {
			log.Warnf("Missing webhook for formId %s", event.Response.FormID)
		}
		sendErr := sender.SendMessage(*webhook, buildMessage(event))
		if sendErr != nil {
			log.Error(sendErr)
			ctx.JSON(500, gin.H{
				"status":  "FAIL",
				"message": "Error sending message to telegram",
			})
		} else {
			ctx.JSON(200, gin.H{
				"status":  "OK",
				"message": "message sent",
			})
		}

	}
}

func buildMessage(event dto.Event) string {
	fieldTitleById := make(map[string]interface{})

	for _, field := range event.Response.Form.Fields {
		fieldTitleById[field.Id] = field.Title
	}
	answers := make([]string, 0)
	for _, answer := range event.Response.Answers {
		switch answer.Type {
		case "text":
			answers = append(
				answers,
				fmt.Sprintf("%s => %s", fieldTitleById[answer.Field.Id], *answer.Text))
		case "email":
			answers = append(
				answers,
				fmt.Sprintf("%s => %s", fieldTitleById[answer.Field.Id], *answer.Email))
		case "choice":
			var v = answer.Choice.Label
			if v == nil {
				v = answer.Choice.Other
			}
			answers = append(
				answers,
				fmt.Sprintf("%s => %s", fieldTitleById[answer.Field.Id], *v))
		case "choices":
			answers = append(
				answers,
				fmt.Sprintf("%s => %s", fieldTitleById[answer.Field.Id], strings.Join(answer.Choices.Labels, ",")))
		case "date":
			answers = append(
				answers,
				fmt.Sprintf("%s => %s", fieldTitleById[answer.Field.Id], *answer.Date))
		case "boolean":
			answers = append(
				answers,
				fmt.Sprintf("%s => %t", fieldTitleById[answer.Field.Id], *answer.Boolean))
		case "number":
			answers = append(
				answers,
				fmt.Sprintf("%s => %d", fieldTitleById[answer.Field.Id], *answer.Number))
		case "file_url":
			answers = append(
				answers,
				fmt.Sprintf("%s => %s", fieldTitleById[answer.Field.Id], *answer.FileURL))
		case "payment":
			answers = append(
				answers,
				fmt.Sprintf("%s => Payment result was succes %t", fieldTitleById[answer.Field.Id], *answer.Payment.Success))

		}
	}
	message := fmt.Sprintf("New response from form: %s\n\n", event.Response.FormID)
	for _, answer := range answers {
		message += answer
		message += "\n"
	}
	return message
}
