package bot

import (
	"teleform/dao"

	log "github.com/sirupsen/logrus"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type MessageSender struct {
	bot tgbotapi.BotAPI
}

func NewMessageSender(bot tgbotapi.BotAPI) MessageSender {
	return MessageSender{
		bot: bot,
	}
}

func (sender MessageSender) SendMessage(webhook dao.Webhook, message string) error {
	log.Infof("Sending %s to %d (%s)", message, webhook.ChatId, webhook.Username)
	_, sendErr := sender.bot.Send(tgbotapi.NewMessage(webhook.ChatId, message))
	return sendErr
}
