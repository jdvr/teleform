package bot

import (
	"fmt"
	"strings"
	"teleform/collections"
	"teleform/dao"
	"teleform/infra/config"

	log "github.com/sirupsen/logrus"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func Polling(bot tgbotapi.BotAPI, webhookDao dao.WebhookDao) {

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, _ := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}

		senderUserName := update.Message.From.UserName
		log.Printf("[%s] %s", senderUserName, update.Message.Text)

		var messageText string
		chatId := update.Message.Chat.ID
		dbWebhook, ok := webhookDao.FindByChatId(chatId)
		if ok {
			if strings.Contains(update.Message.Text, "typeform.com/to") {
				urlParts := strings.Split(update.Message.Text, "/")
				formId := strings.TrimSpace(urlParts[len(urlParts)-1])
				log.Debugf("-%s-", formId)
				if collections.SliceOfStringsContains(dbWebhook.FormIds, formId) {
					messageText = fmt.Sprintf("Your webhook url is %s", config.GetWebhookServerCallback())
				} else {
					_, updateErr := webhookDao.AddFormId(*dbWebhook, formId)
					if updateErr != nil {
						log.Errorf("Error while adding %s to %v => %v", formId, dbWebhook, updateErr)
						messageText = "There was an error generating you webhook :("
					} else {
						messageText = fmt.Sprintf("Your webhook url is %s", config.GetWebhookServerCallback())
					}
				}

			} else if update.Message.Text == "/_data" {
				messageText = fmt.Sprintf("%v", dbWebhook)
			} else {
				messageText = fmt.Sprintf("Hi %s, send me a valid typeform url to generate your webhook url", dbWebhook.Username)
			}
		} else {
			webhook := dao.Webhook{
				ChatId:   chatId,
				Username: senderUserName,
			}
			storedWebhook, createErr := webhookDao.Create(webhook)
			if createErr != nil {
				log.Error(createErr)
			} else {
				log.Debugf("Created webhook %v", storedWebhook)
			}
			messageText = fmt.Sprintf("Missing chat id: You are %s, seding %s, with ChatId %d", senderUserName, update.Message.Text, chatId)
		}
		msg := tgbotapi.NewMessage(chatId, messageText)
		msg.ReplyToMessageID = update.Message.MessageID
		_, sendError := bot.Send(msg)

		if sendError != nil {
			log.Println(sendError)
		}
	}
}
