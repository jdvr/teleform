package main

import (
	"teleform/bot"
	"teleform/infra"
	"teleform/infra/config"
	"teleform/infra/sqlite"
	"teleform/server"

	log "github.com/sirupsen/logrus"

	_ "github.com/joho/godotenv/autoload"
)

func main() {
	infra.InitLogger(config.GetLogLevel())
	botAPI := bot.Create(config.GetBotToken())
	messageSender := bot.NewMessageSender(botAPI)
	webhookDao := sqlite.New("local.db")
	router := server.CreateEngine(messageSender, webhookDao)

	go func() {
		log.Info("Starting bot polling")
		bot.Polling(botAPI, webhookDao)
	}()

	log.Infof("Starting sever")
	_ = router.Run(":" + config.GetPort())
}
