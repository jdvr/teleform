package sqlite

import (
	"database/sql"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"teleform/dao"

	log "github.com/sirupsen/logrus"

	_ "github.com/mattn/go-sqlite3"
)

type WebhookDao struct {
	dbPath string
}

func New(dbPath string) WebhookDao {
	return WebhookDao{dbPath: dbPath}
}

func createTable(db *sql.DB) {
	createStudentTableSQL := `CREATE TABLE IF NOT EXISTS WEBHOOK (
		"ID" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,		
		"USER_NAME" TEXT UNIQUE,
		"CHAT_ID" TEXT,
		"FORM_IDS" TEXT		
	  );`

	statement, err := db.Prepare(createStudentTableSQL)
	if err != nil {
		log.Error(err.Error())
		panic(err)
	}
	_, execErr := statement.Exec()

	if execErr != nil {
		log.Error(execErr)
		panic(execErr)
	}

	log.Debug("webhook table created")
}

func CreateDb(dbPath string) {
	os.Remove(dbPath)

	log.Println("Creating sqlite-database.db...")
	file, err := os.Create(dbPath)
	if err != nil {
		log.Fatal(err.Error())
	}
	file.Close()
	log.Debug(dbPath + " created")
	db, connectErr := connect(dbPath)
	if connectErr != nil {
		log.Fatal(connectErr)
		panic(connectErr)
	}
	createTable(db)
}

func (webhookDao WebhookDao) Create(webhook dao.Webhook) (*dao.Webhook, error) {
	formsIds := ""
	if len(webhook.FormIds) != 0 {
		formsIds = strings.Join(webhook.FormIds, ",")
	}
	result, err := webhookDao.executeStatement(
		"INSERT INTO WEBHOOK(USER_NAME, CHAT_ID, FORM_IDS) VALUES (?, ?, ?)",
		webhook.Username, strconv.FormatInt(webhook.ChatId, 10), formsIds,
	)
	if err != nil {
		return nil, err
	}

	rowId, idErr := result.LastInsertId()

	if idErr != nil {
		return nil, idErr
	}

	return &dao.Webhook{
		Id:       rowId,
		Username: webhook.Username,
		ChatId:   webhook.ChatId,
		FormIds:  strings.Split(formsIds, ","),
	}, nil
}

func (webhookDao WebhookDao) AddFormId(webhook dao.Webhook, formId string) (*dao.Webhook, error) {
	if webhook.Id == 0 {
		return nil, errors.New(fmt.Sprintf("Trying to add form to missing webhook %v", webhook))
	}

	webhook.FormIds = append(webhook.FormIds, formId)

	asString := strings.TrimSpace(strings.Join(webhook.FormIds, ","))

	_, updateErr := webhookDao.executeStatement("UPDATE WEBHOOK SET FORM_IDS=? WHERE ID = ?", asString, webhook.Id)

	if updateErr != nil {
		return nil, updateErr
	}

	return &webhook, nil
}

func (webhookDao WebhookDao) FindByChatId(chatId int64) (*dao.Webhook, bool) {
	webhooks, queryErr := webhookDao.executeQuery("SELECT * FROM WEBHOOK WHERE CHAT_ID = " + strconv.FormatInt(chatId, 10))

	if queryErr != nil {
		log.Error(queryErr)
		return nil, false
	}

	if len(*webhooks) == 0 {
		log.Warnf("Missing webhook for chatId: %d", chatId)
		return nil, false
	}
	return &(*webhooks)[0], true
}

func (webhookDao WebhookDao) FindByFromId(formId string) (*dao.Webhook, bool) {
	webhooks, queryErr := webhookDao.executeQuery(`SELECT * FROM WEBHOOK WHERE FORM_IDS like "%` + formId + `%"`)

	if queryErr != nil {
		log.Error(queryErr)
		return nil, false
	}

	if len(*webhooks) == 0 {
		log.Warnf("Missing webhook for formId: %s", formId)
		return nil, false
	}
	return &(*webhooks)[0], true
}

func (webhookDao WebhookDao) executeQuery(query string) (*[]dao.Webhook, error) {
	db, connectErr := connect(webhookDao.dbPath)
	if connectErr != nil {
		log.Fatal(connectErr)
	}
	defer db.Close()

	log.Infof("Executing %s", query)

	rows, queryErr := db.Query(query)

	if queryErr != nil {
		return nil, queryErr
	}

	defer rows.Close()
	webhooks := make([]dao.Webhook, 0)
	for rows.Next() {
		var id int
		var user_name string
		var chat_id int64
		var form_ids string
		rows.Scan(&id, &user_name, &chat_id, &form_ids)
		webhooks = append(webhooks, dao.Webhook{
			Id:       int64(id),
			Username: user_name,
			ChatId:   chat_id,
			FormIds:  strings.Split(strings.TrimSpace(form_ids), ","),
		})
	}
	return &webhooks, nil
}

func (webhookDao WebhookDao) executeStatement(statement string, args ...interface{}) (sql.Result, error) {
	db, connectErr := connect(webhookDao.dbPath)
	if connectErr != nil {
		log.Fatal(connectErr)
	}
	defer db.Close()

	preparedStatement, prepareErr := db.Prepare(statement)
	log.Debugf("Prepated stment %v", preparedStatement)
	if prepareErr != nil {
		return nil, prepareErr
	}

	result, exeErr := preparedStatement.Exec(args...)

	if exeErr != nil {
		log.Errorf("Execute error %v", exeErr)
		return nil, exeErr
	}

	return result, nil
}

func connect(dbPath string) (*sql.DB, error) {
	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		log.Panic(err)
	}
	return db, err
}
