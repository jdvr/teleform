package infra

import log "github.com/sirupsen/logrus"

func InitLogger(logLevel log.Level) {
	if logLevel != log.DebugLevel {
		log.SetFormatter(&log.JSONFormatter{})
	}

	log.SetLevel(logLevel)
}
