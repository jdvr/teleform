package config

import (
	"os"

	log "github.com/sirupsen/logrus"
)

func GetLogLevel() log.Level {
	level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		return log.WarnLevel
	}
	return level
}

func GetInitDbKey() string {
	return os.Getenv("INIT_DB_KEY")
}

func GetBotToken() string {
	return os.Getenv("BOT_TOKEN")
}

func GetPort() string {
	return os.Getenv("PORT")
}

func GetWebhookServerCallback() string {
	return os.Getenv("WEBHOOK_SERVER_CALLBACK")
}

func GetDatabaseURL() string {
	return os.Getenv("DATABASE_URL")
}
