package main

import (
	"teleform/infra"
	"teleform/infra/config"
	"teleform/infra/postgress"
	"teleform/infra/sqlite"

	log "github.com/sirupsen/logrus"
)

func main() {
	infra.InitLogger(log.DebugLevel)
	dbURL := config.GetDatabaseURL()
	if dbURL != "" {
		log.Info("Initializing PostgresSql DB")
		postgress.CreateDb(dbURL)
	} else {
		log.Info("Initializing SQL Lite temp DB")
		sqlite.CreateDb("local.db")
	}
}
