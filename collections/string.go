package collections

func SliceOfStringsContains(collection []string, value string) bool {
	for _, item := range collection {
		if item == value {
			return true
		}
	}
	return false
}
