.PHONY: start build run-bot run-server ngrok

VERSION ?= local

build:
	docker build -t registry.gitlab.com/jdvr/teleform:$(VERSION) .

start:
	docker compose up

shell:
	docker compose build
	docker compose run --rm  --service-ports teleform /bin/ash

down:
	docker-compose down

ngork:
	ngrok http 3333

heroku_deploy:
	heroku container:push web
	heroku container:release web