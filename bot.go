package main

import (
	"fmt"
	"teleform/bot"
	"teleform/infra"
	"teleform/infra/config"
	"teleform/infra/sqlite"

	_ "github.com/joho/godotenv/autoload"
)

func main() {
	fmt.Println("Hi")
	infra.InitLogger(config.GetLogLevel())
	bot.Polling(bot.Create(config.GetBotToken()), sqlite.New("local.db"))
}
