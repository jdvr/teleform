package dao

type Webhook struct {
	Id       int64
	Username string
	ChatId   int64
	FormIds  []string
}

type WebhookDao interface {
	Create(webhook Webhook) (*Webhook, error)
	AddFormId(webhook Webhook, formId string) (*Webhook, error)
	FindByChatId(chatId int64) (*Webhook, bool)
	FindByFromId(formId string) (*Webhook, bool)
}
