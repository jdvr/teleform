package main

import (
	"teleform/bot"
	"teleform/infra"
	"teleform/infra/config"
	"teleform/infra/sqlite"
	"teleform/server"

	_ "github.com/joho/godotenv/autoload"
)

func main() {
	infra.InitLogger(config.GetLogLevel())
	botAPI := bot.Create(config.GetBotToken())
	messageSender := bot.NewMessageSender(botAPI)
	webhookDao := sqlite.New("local.db")
	engine := server.CreateEngine(messageSender, webhookDao)
	_ = engine.Run(":6000")
}
