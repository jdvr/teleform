module teleform

// +heroku goVersion go1.16
go 1.16

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.10.0
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/sirupsen/logrus v1.8.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
